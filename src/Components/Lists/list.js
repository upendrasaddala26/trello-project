import { Component } from "react";
import { connect } from "react-redux"

import { getListsData, postNewList, deleteList } from "../../Redux/actions"

import Cards from "../Cards/cards";
import CardDetails from "../CardDetails/cardDetails";

import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';

import "./list.css"
class Lists extends Component {
    state = {cardName:"", showCardId:"", enterListInputIsShow: false, newListName: "", isPopUpShow: false }

    componentDidMount() {
        const { boardId } = this.props.match.params
        this.props.getListsData(boardId)
    }

    addList = () => {
        const { newListName } = this.state
        const { boardId } = this.props.match.params
        this.props.postNewList(boardId, newListName)
        this.setState({ newListName: "" })
    }

    closeCardTitleInput = () => {
        this.setState({ enterListInputIsShow: false })
    }

    onAddCardInput = (event) => {
        this.setState({ newListName: event.target.value })
    }

    openCardTitleInput = () => {
        this.setState({ enterListInputIsShow: true })
    }

    displayListInputCard = () => {
        const { enterListInputIsShow, newListName } = this.state
        if (enterListInputIsShow) {
            return (
                <div className="list-input-card">
                    <input value={newListName} type="text" onChange={this.onAddCardInput} className="text-area-input" placeholder="Enter a list.." />
                    <div className="add-close-buttons">
                        <button onClick={this.addList} className="add-button-card">Add List</button>
                        <button onClick={this.closeCardTitleInput} className="add-card-close"><CloseIcon /></button>
                    </div>
                </div>
            )
        }

        return (
            <button onClick={this.openCardTitleInput} className="add-list-button">
                <AddIcon />
                Add a new list
            </button>

        )
    }

    deleteListItem = (id) => {
        this.props.deleteList(id)
    }

    showPopUp = (cardId,cardName) =>{
        this.setState({isPopUpShow:true,showCardId:cardId,cardName})
    }

    closePopUp = () =>{
        this.setState({isPopUpShow:false})
    }

    displyPopUp = () =>{
        const {isPopUpShow,showCardId,cardName} = this.state
        if(isPopUpShow){
            return(
                <CardDetails cardName={cardName} showCardId={showCardId} closePopUp={this.closePopUp}/>
            )
        }
    }

    render() {
        const { lists } = this.props

        return (
            <div className="list-main-container">
                <div className="list-container">
                    {lists.map(eachList =>
                        <div key={eachList.id} className="eachlist-details">
                            <div className="list-header">
                                <h4>{eachList.name}</h4>
                                <button onClick={() => this.deleteListItem(eachList.id)} className="add-card-close">
                                    <DeleteIcon />
                                </button>
                            </div>
                            <Cards listId={eachList.id} showPopUp={this.showPopUp}/>
                        </div>
                    )}
                    {this.displayListInputCard()}
                </div>
                 {this.displyPopUp()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        lists: state.lists.lists
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getListsData: (boardId) => dispatch(getListsData(boardId)),
        postNewList: (boardId, newListName) => dispatch(postNewList(boardId, newListName)),
        deleteList: (id) => dispatch(deleteList(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Lists)