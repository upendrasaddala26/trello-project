import { Component } from "react";
import { connect } from "react-redux";

import { addNewCheckItem,deleteCheckItem,updateCheckItem } from "../../Redux/actions";

import "./checkItems.css"

import HighlightOffIcon from '@mui/icons-material/HighlightOff';
class CheckItems extends Component {
    state = {addCheckItemInput:false, checkItemuserInput:""}


    onChangeItemInputStatus = () =>{
        this.setState({addCheckItemInput:true})
    }

    closeCheckItemInput = () =>{
        this.setState({addCheckItemInput:false})
    }

    addCheckItemToData = () =>{
        const {checkItemuserInput} = this.state
        const { checklistId,addNewCheckItem } = this.props
        
        addNewCheckItem(checklistId,checkItemuserInput)

        this.setState({checkItemuserInput:""})
    }
                          
    CheckItemInput = (event) =>{
        this.setState({checkItemuserInput:event.target.value})
    }

    showCheckItemInput = () => {
        const { addCheckItemInput ,checkItemuserInput} = this.state
        
        if (addCheckItemInput) {
            return (
                <div className="checkItem-input-container">
                    <textarea value={checkItemuserInput} onChange={this.CheckItemInput} className="item-input-text" placeholder="Enter CheckItem"></textarea>
                    <br />
                    <button onClick={this.addCheckItemToData} className="checkItem-Add-btn">Add</button>
                    <button onClick={this.closeCheckItemInput} className="checkItem-close-btn">Close</button>
                </div>
            )
        } else {
            return (
                <button onClick={this.onChangeItemInputStatus} className="add-checkItem-button">Add an item</button>
            )
        }
    }

    deleteCheckItem = (checkItemId) =>{
        const {checklistId,deleteCheckItem} = this.props
        deleteCheckItem(checklistId,checkItemId)
    }
    
    checkEl = (checkItemId) =>{
        const {cardId,checkItems,updateCheckItem,checklistId} = this.props
        let value 
        checkItems.forEach(eachItem => {
            if(eachItem.id === checkItemId){
                if(eachItem.state === "complete"){
                    value = "incomplete"
                }else{
                    value = "complete"
                }
            }
        });
        updateCheckItem(cardId,checkItemId,value,checklistId)

        
    }
    

    render() {
        const { checkItems } = this.props

        return (
            <>
             {checkItems.map(eachItem =>{
               
                let checklistCheck = eachItem.state === "complete" ? 'isChecked' : ""
                let isChecked = eachItem.state === "complete" ? 'checked' : ""
                return(
                 <div className="checkItems" key={eachItem.id}>
                 <input defaultChecked={isChecked} onClick={() => this.checkEl(eachItem.id)} type="checkbox" className="checkitem-checkbox" />
                 <div className="label-text-card">
                     <label className={`checkitem-text ${checklistCheck}`}>{eachItem.name}</label>
                     <button onClick={() =>this.deleteCheckItem(eachItem.id)} className="card-delete-button">
                     <HighlightOffIcon/>
                     </button>
                 </div>
             </div>)
    })}
              {this.showCheckItemInput()}
            </>
           
        )
    }
}

const mapDispatchToProps = (dispatch)=>{
      return{
        addNewCheckItem: (checklistId,checkItemuserInput) => dispatch(addNewCheckItem(checklistId,checkItemuserInput)),
        deleteCheckItem: (checklistId,checkItemId) => dispatch(deleteCheckItem(checklistId,checkItemId)),
        updateCheckItem: (cardId,checkItemId,value,checklistId) => dispatch(updateCheckItem(cardId,checkItemId,value,checklistId))
      }
}

export default connect(null,mapDispatchToProps)(CheckItems)

