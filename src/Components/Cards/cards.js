import { Component } from "react";
import { connect } from "react-redux";

import "./cards.css"

import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';

import { fetchCards, addNewCard,deleteCard } from "../../Redux/actions"




class Cards extends Component {
    state = { enterCardInputIsShow: false, addCardInput: "" }

    componentDidMount() {
        const { listId, fetchCards } = this.props
        fetchCards(listId)
    }

    onAddCardInput = (event) => {
        this.setState({ addCardInput: event.target.value })
    }

    addCardItem = () => {
        const { addCardInput } = this.state
        const { listId, addNewCard } = this.props
        addNewCard(listId, addCardInput)
        this.setState({ addCardInput: ""})
    }

    closeCardTitleInput = () => {
        this.setState({ enterCardInputIsShow: false })
    }

    openCardTitleInput = () => {
        this.setState({ enterCardInputIsShow: true })
    }

    enterCardText = () => {
        const { enterCardInputIsShow, addCardInput } = this.state
        if (enterCardInputIsShow) {
            return (
                <div>
                    <textarea value={addCardInput} onChange={this.onAddCardInput} className="text-area-input" placeholder="Enter a tilte for this card.." rows={3} cols={30} ></textarea>
                    <div className="add-close-buttons">
                        <button onClick={this.addCardItem} className="add-button-card">Add card</button>
                        <button onClick={this.closeCardTitleInput} className="add-card-close"><CloseIcon /></button>
                    </div>
                </div>
            )
        }
        return (
            <button onClick={this.openCardTitleInput} className="add-card-button">
                <AddIcon />
                Add a card
            </button>

        )
    }

    onCardDetailsShow = (cardId,cardName) => {
        this.props.showPopUp(cardId,cardName)
    }

    deleteCard = (id) =>{
        const { listId } = this.props
        this.props.deleteCard(id,listId)
    }

    render() {
        const { cards, listId } = this.props
        return (
            <>
                {cards[listId] && cards[listId].map(item =>
                    <div key={item.id} className="card-main-container">
                        <div onClick={() => this.onCardDetailsShow(item.id,item.name)} className="card-container" >
                            <h5 className="card-name">{item.name}</h5>
                        </div>
                        <button onClick={() => this.deleteCard(item.id)} className="card-delete-button">
                            <DeleteIcon />
                        </button>
                    </div>
                )}

                {this.enterCardText()}
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cards: state.cards.cards
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCards: (listId) => dispatch(fetchCards(listId)),
        addNewCard: (id, addCardInput) => dispatch(addNewCard(id, addCardInput)),
        deleteCard: (id,listId) => dispatch(deleteCard(id,listId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cards)