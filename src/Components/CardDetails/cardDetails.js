import { Component } from "react";

import "./cardDetails.css"

import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';

import CheckItems from "../CheckItems/checkItems";

import { connect } from "react-redux";

import { fetchCheckList,addNewCheckList,deleteCheckList} from "../../Redux/actions"

class CardDetails extends Component{

    state = {checkListInputShow: false,checklistInput:""}

    componentDidMount(){
        const {showCardId} = this.props
        this.props.fetchCheckList(showCardId)
    }

    closePopup =() =>{
        this.props.closePopUp()
    }
    
    closeUserInput = () =>{
        this.setState({checkListInputShow:false})
    }

    showChecklistAdd = () =>{
        this.setState({checkListInputShow:true})
    }

    newCheckList = (event) =>{
         this.setState({checklistInput:event.target.value})
    }

    addCheckListInput = () =>{
        const {checklistInput} = this.state
        const {showCardId,addNewCheckList} = this.props
        addNewCheckList(showCardId,checklistInput)
        this.setState({checklistInput:""})
    }
     
    deleteCheckList = (id) =>{
        this.props.deleteCheckList(id)
    }

    addCheckList = () => {
        const { checkListInputShow, checklistInput } = this.state

        if (checkListInputShow) {
            return (
                <div className="main-checklist-card">
                    <div className="checklist-input">
                        <div className="checklist-add-header-card">
                            <p className="add-checklist-head">Add CheckList</p>
                            <button onClick={this.closeUserInput} className="add-card-close card-details-closebtn">
                                <CloseIcon className="close-checkin" />
                            </button>
                        </div>
                        <hr className="line" />
                        <input  onChange={this.newCheckList} value={checklistInput} placeholder="Enter" className="input-text" type="text" />
                        <br />
                        <button onClick={this.addCheckListInput} className="add-checklist-button2">Add</button>
                    </div>
                </div>
            )
        } else {
            return null
        }
    }



    render(){

        const {checkList,cardName} = this.props

        return(
            <div className="popup-main-container">
            <div className="popup-datacard">
                <div className="checklist-details-header">
                    <h2 className="card-heading">{cardName}</h2>
                    <button onClick={this.closePopup} className="add-card-close"><CloseIcon /></button>
                </div>

                <button onClick={this.showChecklistAdd} className="add-checklist-button">Add CheckList</button>
                {this.addCheckList()}

                <ul className="checkLists">
                    {checkList.map(item => {
                        console.log(item.name)
                        return (
                            <li key={item.id}>
                                <div className="check-item-card">
                                    <h4>{item.name}</h4>
                                    <button onClick={() => this.deleteCheckList(item.id)} className="add-card-close">
                                    <DeleteIcon/>
                                    </button>
                                </div>
                                <hr />
                                <CheckItems cardId={this.props.showCardId} checkItems={item.checkItems} checklistId={item.id} />
                            </li>
                        )
                    })}
                    
                </ul>
               
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) =>{
    return{
       checkList: state.checkList.checkList
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        fetchCheckList: (showCardId) => dispatch(fetchCheckList(showCardId)),
        addNewCheckList: (cardId,newChecklist) => dispatch(addNewCheckList(cardId,newChecklist)),
        deleteCheckList: (id) => dispatch(deleteCheckList(id))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(CardDetails)