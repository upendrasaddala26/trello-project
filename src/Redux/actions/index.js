import axios from "axios"
import { FETCH_BOARDS_REQUEST, FETCH_BOARDS_SUCCESS, FETCH_BOARDS_FAIL, FETCH_CARDS_SUCCESS, POST_BOARD, FETCH_LISTS_DATA, POST_LIST, DELETE_LIST, ADD_NEW_CARD, DELETE_CARD, FETCH_CHECKLISTS, ADD_NEW_CHECKLIST, DELETE_CHECKLIST, ADD_NEW_CHECKITEM,DELETE_CHECKITEM ,UPDATE_CHECKITEM} from "./actionTypes"

const ApiKey = "dc336cbd741eb8db35bfe2dd78eff829"
const ApiToken = "a67df0fb848c379fba9148cd2cbbc7147278af0979962c2e92211ed1ffaaea02"


export const fetchBoardsRequest = () => {
    return {
        type: FETCH_BOARDS_REQUEST
    }
}

export const fetchBoardsSuccess = (users) => {
    return {
        type: FETCH_BOARDS_SUCCESS,
        data: users
    }
}

export const fetchBoardsFail = (error) => {
    return {
        type: FETCH_BOARDS_FAIL,
        data: error
    }
}

export const fetchBorders = () => {
    return function (dispatch) {
        dispatch(fetchBoardsRequest())
        axios.get(`https://api.trello.com/1/members/me/boards?key=${ApiKey}&token=${ApiToken}`)
            .then(res => {
                dispatch(fetchBoardsSuccess(res.data))
            })
            .catch(error => {
                dispatch(fetchBoardsFail(error))
                console.log(error)
            })
    }
}

export const postBoard = (newBoard) => {
    return function (dispatch) {
        axios.post(`https://api.trello.com/1/boards/?name=${newBoard}&key=${ApiKey}&token=${ApiToken}`)
            .then(res => {
                dispatch({
                    type: POST_BOARD,
                    payload: res
                })
            }).catch(err => {
                console.log(err)
            })
    }
}

export const getListsData = (id) => {
    return function (dispatch) {
        axios.get(`https://api.trello.com/1/boards/${id}/lists?key=${ApiKey}&token=${ApiToken}`)
            .then(res => {
                dispatch({
                    type: FETCH_LISTS_DATA,
                    payload: res.data
                })
            }).catch(error => {
                console.log(error)
            })
    }
}

export const postNewList = (boardId, newListName) => {
    return function (dispatch) {
        axios.post(`https://api.trello.com/1/lists?name=${newListName}&idBoard=${boardId}&key=${ApiKey}&token=${ApiToken}`)
            .then(res => {
                dispatch({
                    type: POST_LIST,
                    payload: res.data
                })
            }).catch(error => {
                console.log(error)
            })
    }
}

export const deleteList = (id) => {
    return function (dispatch) {
        axios.put(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${ApiKey}&token=${ApiToken}`)
            .then(res => {
                dispatch({
                    type: DELETE_LIST,
                    payload: id
                })
            })
    }
}

export const fetchCards = (id) => {
    return function (dispatch) {
        axios.get(`https://api.trello.com/1/lists/${id}/cards?key=${ApiKey}&token=${ApiToken}`)
            .then(res => {
                dispatch({
                    type: FETCH_CARDS_SUCCESS,
                    listId: id,
                    payload: res.data
                })
            })
            .catch(error => {
                console.log(error)
            })
    }
}

export const addNewCard = (id, newCard) => {
    return function (dispatch) {
        axios.post(`https://api.trello.com/1/cards?idList=${id}&name=${newCard}&key=${ApiKey}&token=${ApiToken}`)
            .then(res => {
                dispatch({
                    type: ADD_NEW_CARD,
                    listId: id,
                    payload: res.data
                })
            }).catch(error => {
                console.log(error)
            })
    }
}

export const deleteCard = (id,listId) =>{
    return function (dispatch){
        axios.delete(`https://api.trello.com/1/cards/${id}?key=${ApiKey}&token=${ApiToken}`)
        .then(res =>{
            dispatch({
                type:DELETE_CARD,
                cardId:id,
                listId:listId
            })
        }).catch(error =>{
            console.log(error)
        })
    }
}

export const fetchCheckList = (cardId) =>{
    return function (dispatch){
        axios.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${ApiKey}&token=${ApiToken}`)
        .then(res =>{
            dispatch({
                type:FETCH_CHECKLISTS,
                payload:res.data
            })
        }).catch(error =>{
            console.log(error)
        })
    }
}

export const addNewCheckList = (cardId,checklistInput) =>{
    return function (dispatch){
        axios.post(`https://api.trello.com/1/checklists?name=${checklistInput}&idCard=${cardId}&key=${ApiKey}&token=${ApiToken}`)
        .then(res =>{
            dispatch({
                type:ADD_NEW_CHECKLIST,
                payload:res.data
            })
        }).catch(error =>{
            console.log(error)
        })
    }
}

export const deleteCheckList = (checkListId) =>{
    return function (dispatch){
        axios.delete(`https://api.trello.com/1/checklists/${checkListId}?key=${ApiKey}&token=${ApiToken}`)
        .then(res =>{
            dispatch({
                type:DELETE_CHECKLIST,
                checkListId
            })
        }).catch(error =>{
            console.log(error)
        })
    }
}


export const addNewCheckItem = (checklistId,newCheckItem)=>{
    return function(dispatch){
        axios.post(`https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${newCheckItem}&key=${ApiKey}&token=${ApiToken}`)
        .then(res =>{
            dispatch({
                type:ADD_NEW_CHECKITEM,
                payload:res.data,
                checkListId:checklistId
            })
        }).catch(error =>{
            console.log(error)
        })
    }
}

export const deleteCheckItem = (checkListId,checkItemId) =>{
    return function(dispatch){
        axios.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${ApiKey}&token=${ApiToken}`)
        .then(res =>{
                dispatch({
                    type:DELETE_CHECKITEM,
                    checkListId,
                    checkItemId
                })
        }).catch(error =>{
            console.log(error)
        })
    }
}

export const updateCheckItem = (cardId,CheckItemid,value,checkListId) =>{
    return function(dispatch){
        axios.put(`https://api.trello.com/1/cards/${cardId}/checkItem/${CheckItemid}?state=${value}&key=${ApiKey}&token=${ApiToken}`)
        .then(res =>{
            dispatch({
                type:UPDATE_CHECKITEM,
                checkListId,
                CheckItemid,
                value

            })
        }).catch(error =>{
            console.log(error)
        })
    }
}
