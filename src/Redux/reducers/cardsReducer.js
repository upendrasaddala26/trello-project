
import { ADD_NEW_CARD, DELETE_CARD, FETCH_CARDS_SUCCESS } from "../actions/actionTypes"

const initialState = {
    cards: {}
}

const cardsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CARDS_SUCCESS:
            const newObj = {...state.cards}
            newObj[action.listId] = action.payload
           return {...state,cards:newObj}
        case ADD_NEW_CARD:
            const data = state.cards[action.listId].concat(action.payload)
            const AddnewCard = {...state.cards, [action.listId]:data}
            return {...state, cards:AddnewCard}
        case DELETE_CARD:
            const deletedCardList = state.cards[action.listId]
            const restCards = deletedCardList.filter(eachcard =>{
                return eachcard.id !== action.cardId
            })
            const newCards = {...state.cards, [action.listId]:restCards}
            return {...state, cards:newCards}
        default:
            return state
    }
}

export default cardsReducer




