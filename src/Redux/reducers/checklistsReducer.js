import { ADD_NEW_CHECKLIST, DELETE_CHECKLIST, FETCH_CHECKLISTS ,ADD_NEW_CHECKITEM,DELETE_CHECKITEM, UPDATE_CHECKITEM} from "../actions/actionTypes"

const initialState = {
    checkList: []
}

const checkListReducer = (state=initialState,action)=>{
    switch(action.type){
        case FETCH_CHECKLISTS:
            const checkList = action.payload
            return {...state,checkList}
        case ADD_NEW_CHECKLIST:
            const newList = state.checkList.concat(action.payload)
            return {...state,checkList:newList}
        case DELETE_CHECKLIST:
            const restCheckList = state.checkList.filter(eachCheckList =>{
                return eachCheckList.id !== action.checkListId
            })
            return {...state,checkList:restCheckList}
        case ADD_NEW_CHECKITEM:

            const addedNewCheckItem = state.checkList.reduce((acc,eachItem) =>{
                   if(eachItem.id === action.checkListId){
                    const itemAdd = [...eachItem.checkItems,action.payload]
                     const data = {...eachItem,checkItems:itemAdd}
                     acc.push(data)
                   }else{
                    acc.push(eachItem)
                   }
                   return acc
            },[])
            return {...state,checkList:addedNewCheckItem}

        case DELETE_CHECKITEM:
            const removeCheckItem = state.checkList.reduce((acc,eachItem) =>{
                if(eachItem.id === action.checkListId){
                     const removeItem = eachItem.checkItems.filter(item =>{
                        return item.id !== action.checkItemId
                     })
                     const data = {...eachItem,checkItems:removeItem}
                     acc.push(data)
                   }else{
                    acc.push(eachItem)
                   }
                return acc
         },[])
            return {...state,checkList:removeCheckItem}
        
        case UPDATE_CHECKITEM:
           const updateCheckItemState = state.checkList.reduce((acc,eachItem) =>{
              if(eachItem.id === action.checkListId){
                 const newCheckItems = eachItem.checkItems.map(item =>{
                    if(item.id === action.CheckItemid){
                        return {...item,state: action.value}
                    }else{
                        return item
                    }
                 })
                 const data = {...eachItem,checkItems:newCheckItems}
                 acc.push(data)
              }else{
                acc.push(eachItem)
              }
              return acc
           },[])
           return {...state,checkList:updateCheckItemState}
        default:
            return state
    }
}

export default checkListReducer