import { applyMiddleware, legacy_createStore as createStore ,combineReducers} from "redux"

import boardReducers  from "./reducers/boardsReducer"
import cardsReducer from "./reducers/cardsReducer"
import listsReduces from "./reducers/listsReducer"
import checkListReducer from "./reducers/checklistsReducer"

import thunk from "redux-thunk"

const combainState = combineReducers({
   boards:boardReducers,
   lists:listsReduces,
   cards:cardsReducer,
   checkList: checkListReducer
})

const store = createStore(combainState, applyMiddleware(thunk))

export default store